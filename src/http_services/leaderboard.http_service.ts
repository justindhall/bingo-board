import {CrudHttpService} from './crud.http_service';

export class LeaderboardHttpService extends CrudHttpService {
  constructor() {
    super('leaderboards');
  }
}
