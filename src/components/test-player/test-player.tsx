import {Component, h, State, Prop, EventEmitter, Event, Listen} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'test-player',
})
export class TestPlayer {

  @Prop() player: {id: number, name: string, img: string, wins: number};

  @Event() playerDeleted: EventEmitter;
  @Event() playerEdited: EventEmitter;
  @Event() winAdded: EventEmitter;
  @Event() winSubtracted: EventEmitter;

  @State() editing: boolean = false;

  newPlayer: string;
  itemSlider: HTMLIonItemSlidingElement;

  @Listen('keydown')
  handleKeyDown(ev: KeyboardEvent){
    if (ev.key === 'Enter'){
      this.updated()
    }
  }

  toggle() {
    this.editing = !this.editing
  }

  destroy() {
    this.playerDeleted.emit({id: this.player.id})
    this.itemSlider.close()
  }

  valueChanged(event) {
    this.newPlayer = event.target.value;
  }

  updated() {
    if (this.newPlayer != '') {
      this.playerEdited.emit({name: this.newPlayer, id: this.player.id, img: this.player.img, wins: this.player.wins})
      this.toggle();
      this.itemSlider.close()
    } else {
      alert("Input cannot be empty")
    }
  }

  incrementWins() {
    this.winAdded.emit({id: this.player.id, wins: this.player.wins + 1});
  }

  decrementWins() {
    if (this.player.wins > 0){
      this.winSubtracted.emit({id: this.player.id, wins: this.player.wins - 1});
    }
  }

  render() {
    if(this.editing) {
      return [
        <ion-item>
          <ion-input placeholder={this.player.name} onInput={(event) => { this.valueChanged(event) }} />
          <ion-button slot='end' onClick={() => { this.updated()}}>
            <ion-icon name="save"/>
          </ion-button>
        </ion-item>
      ];
    } else {
      return [
          <ion-item-sliding ref={el => this.itemSlider = el as HTMLIonItemSlidingElement}>
            <ion-item-options side="start">
              <ion-item-option onClick={() => this.toggle()}>
                Edit
              </ion-item-option>
              <ion-item-option color={'danger'} onClick={() => {this.destroy()}}>
                Delete
              </ion-item-option>
            </ion-item-options>
            <ion-item lines="none">
              <ion-avatar slot="start">
               <ion-img src={this.player.img} />
              </ion-avatar>
              <ion-text>
                {this.player.name}
              </ion-text>
              <label slot="end">{this.player.wins}</label>
              <ion-buttons slot='end'>
                <ion-button slot="end" onClick={() => this.incrementWins() }>
                  <ion-icon name="add" />
                </ion-button>
                <ion-button slot="end" onClick={() => this.decrementWins() }>
                  <ion-icon name="remove" />
                </ion-button>
              </ion-buttons>
            </ion-item>
          </ion-item-sliding>
      ]
    }
  }
}
