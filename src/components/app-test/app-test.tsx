import {Component, h, State, Listen} from '@stencil/core';
import _ from 'underscore';
import {LeaderboardHttpService} from "../../http_services/leaderboard.http_service";

@Component({
  tag: 'app-test',
})
export class AppTest {

  @State() leaderboard: any[];
  @State() addingPlayer: boolean = true;

  async componentWillLoad() {
    const returningPlayers = await new LeaderboardHttpService().getRequest('https://bingo-leaderboard.herokuapp.com/leaderboards')
    this.leaderboard = returningPlayers;
  }

  async create(player) {
    const response = await new LeaderboardHttpService().create({name: player.name, img: player.img, wins: player.wins})
    this.leaderboard = [...this.leaderboard, response]
  }

  async delete(id) {
    await new LeaderboardHttpService().delete(id);
    const foundPlayer = _.findWhere(this.leaderboard, {id: id});
    const index = _.indexOf(this.leaderboard, foundPlayer);
    const copy = [...this.leaderboard];
    delete copy[index];
    this.leaderboard = copy;
  }

  async update(player) {
    const foundPlayer = _.findWhere(this.leaderboard, {id: player.id});
    const index = _.indexOf(this.leaderboard, foundPlayer);
    const response = await new LeaderboardHttpService().update(player);
    const copy = [...this.leaderboard];
    copy[index] = response;
    this.leaderboard = copy;
  }

  toggle() {
    this.addingPlayer = !this.addingPlayer
  }

  @Listen('playerAdded')
  add(event) {
      this.create(event.detail);
      this.toggle;
  }

  @Listen('playerDeleted')
  destroy(event) {
    this.delete(event.detail.id);
  }

  @Listen('playerEdited')
  edit(event) {
    this.update(event.detail)
  }

  @Listen('winAdded')
  addWin(event) {
      this.update(event.detail)
  }

  @Listen('winSubtracted')
  subtractWin(event) {
    this.update(event.detail)
  }

  renderLeaderboard() {
    const halfSorted = _.sortBy(this.leaderboard, "name").reverse();
    const fullSorted = _.sortBy(halfSorted, "wins").reverse();
    return fullSorted.map((player)=> {
      return [
        <test-player player={player}/>
      ]
    })
  }

  render() {
    if (this.addingPlayer) {
      return [
        <app-header/>,
        <ion-content>
          <ion-card>
            <ion-list>
              {this.renderLeaderboard()}
            </ion-list>
          </ion-card>
          <ion-fab vertical="bottom" horizontal="end" slot="fixed">
            <ion-fab-button onClick={() => {this.toggle()}}>
              <ion-icon name="add" />
            </ion-fab-button>
          </ion-fab>
        </ion-content>
      ];
    } else {
      return [
        <app-header />,
        <ion-content>
          <ion-card>
            <ion-list>
              {this.renderLeaderboard()}
            </ion-list>
          </ion-card>
        </ion-content>,
        <test-add-player />
        ]
    }
  }
}
