import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-header',
})
export class AppHeader {

  render() {
    return [
      <ion-header padding-top>
        <ion-title>
          <ion-label>
            Bingo Night
          </ion-label>
        </ion-title>
      </ion-header>
    ]
  }
}
