import {Component,
  h,
  Prop,
  EventEmitter,
  Event,
  Listen} from '@stencil/core';
import _ from 'underscore';

@Component({
  tag: 'test-add-player',
})
export class TestAddPlayer {

  imgArray: any[] = ["../../assets/icon/logo.png",
    "https://images.unsplash.com/photo-1500995617113-cf789362a3e1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1560315418-99913cddb700?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1560249952-fc2ff2effd10?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1558986377-c44f6a2b50f0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1559496417-e7f25cb247f3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1560371712-86a52593e8fc?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1559762717-99c81ac85459?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1559242593-4dedc2dd4767?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
    "https://images.unsplash.com/photo-1560292290-762a68f36318?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"];

  @Prop() newPlayer: string;

  @Event() playerAdded: EventEmitter;

  @Listen('keydown')
  handleKeyDown(ev: KeyboardEvent){
    if (ev.key === 'Enter'){
      this.addPlayer()
    }
  }

  valueChanged(event) {
    this.newPlayer = event.target.value;
  }

  resetField() {
    this.newPlayer = ''
  }

  addPlayer() {
    if (this.newPlayer != '') {
      this.playerAdded.emit({
        name: this.newPlayer,
        img: _.sample(this.imgArray),
        wins: 0})
      this.resetField();
    }
  }

  render() {
    return [
      <ion-footer>
        <ion-toolbar>
          <ion-item>
            <ion-input placeholder="Name" value={this.newPlayer} onInput={(event) => { this.valueChanged(event) }} />
            <ion-label>
              <ion-button slot="end" onClick={() => this.addPlayer()}>
                <ion-icon name="person-add" />
              </ion-button>
            </ion-label>
          </ion-item>
        </ion-toolbar>
      </ion-footer>
    ]
  }
}
